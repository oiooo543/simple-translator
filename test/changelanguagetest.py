import unittest

from src.Trans import LanguageTranslate


class TestLanguageChange(unittest.TestCase):

    def test_language_with_default(self):
        trans = LanguageTranslate()

        pet = trans.changelanguage('안녕하세요.')
        self.assertTrue(pet.text, 'Good evening.')

    def test_with_src_parameters(self):
        trans = LanguageTranslate()

        pet = trans.changelanguage('veritas lux mea', 'la')
        self.assertTrue(pet.text, 'The truth is my light')

    def test_with_src_dest_parameters(self):
        trans = LanguageTranslate()
        pet = trans.changelanguage('안녕하세요', 'ko', 'ja')
        self.assertTrue(pet.text, 'こんにちは')

